import Head from "next/head";
import Link from "next/link";
import React from "react";
import {FaPhoneAlt} from 'react-icons/fa';
import {AiOutlineMenu} from 'react-icons/ai'

export default function PageLayout ({ children, title = 'IDEЯ WEB STUDIO' }) {

    return (
        <>
            <Head>
                <title>{title}</title>
                <link rel="shortcut icon" href="/favicon.png" type="image/x-icon" />
                <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;700&display=swap" rel="stylesheet" />
                <link href="https://fonts.googleapis.com/css2?family=Open+Sans&family=Oswald:wght@300;700&display=swap" rel="stylesheet" />
            </Head>
            <div className="min-h-screen flex flex-col justify-between bg-black font-open-sans" >

                <main className="flex-grow">{ children }</main>

            </div>
        </>

    )
}