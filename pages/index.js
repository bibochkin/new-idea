import PageLayout from '../components/Layouts/Layout';
import React from 'react';
import 'animate.css';

export default function Index () {

    return (
        <PageLayout title={"IDEЯ WEB STUDIO"}>
            <div className="text-8xl text-white font-open-sans p-4">
                <div className="flex flex-row">
                    <p className="animate-fade-in-down-1">I</p>
                    <p className="animate-fade-in-down-2">D</p>
                    <p className="animate-fade-in-down-3">E</p>
                    <p className="animate-fade-in-down-4">Я</p>
                </div>
                <h1 className="text-4xl animate__fadeInUp animate__animated animate__slow">WEB STUDIO</h1>
            </div>
        </PageLayout>

    )
}